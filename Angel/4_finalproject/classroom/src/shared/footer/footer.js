import React from 'react'; 
import './footer.css';



function Footer(){
    const currentYear = new Date ().getFullYear(); 
    //console.log ('current year' , currentYear);
    return(
        <footer className="navbar navbar-light light-blue fixed-bottom">
            <p className=" "> &copy; MissionCode{currentYear}</p>
        </footer>
    )
}

export default Footer; 