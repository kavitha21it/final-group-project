//**control shirt L change className  to className */
import React from 'react';
import { BrowserRouter } from "react-router-dom";
import {
    Link
} from 'react-router-dom';

//import React from '//place mystlye css  path here//'; 

function NavHome() {
    return ( //**place Nav script here*/
        <nav className="navbar navbar-expand-lg navbar-dark bg-info mb-5">
            <a className="navbar-brand" href="https://www.missioncode.org/"> Welcome to Missioncode</a>

            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarText">
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item active">
                         <Link className="nav-link" to="/Home_pg">Home <span className="sr-only">(current)</span></Link> 
                    </li>
                    <li className="nav-item active">
                         <Link className="nav-link" to="/Job_List_pg">Assignments</Link> 
                    </li>
                    <li className="nav-item active">
                         <Link className="nav-link" to="/Home_pg">Logout</Link> 
                    </li>
                    <li className="nav-item">
                     <a  className="nav-link" href="mailto:mailto:MissionCodeSupport@gmail.com?subject = Feedback&body = Message">Support</a>
                    </li>
                </ul>
            </div>
        </nav>
    )
}
export default NavHome;