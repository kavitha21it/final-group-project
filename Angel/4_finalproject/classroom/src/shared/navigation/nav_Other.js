//**control shirt L change class to className */
import React from 'react';
import { BrowserRouter } from "react-router-dom";
import {
    Link
} from 'react-router-dom';

function NavOther() {
    return ( //**place Nav script here*/
        <nav class="navbar navbar-expand-lg navbar-dark bg-info">
            <img src="http://www.missioncode.co/img/logo.png" height="65" width="160" class="img-fluid mb-md-n1" />
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item active">
                        <BrowserRouter> <Link className="nav-link" to="/home_pg">Home <span className="sr-only">(current)</span></Link> </BrowserRouter>
                    </li>
                    <li className="nav-item active">
                        <BrowserRouter> <Link className="nav-link" to="/Job_List_pg">Assignments</Link> </BrowserRouter>
                    </li>
                    <li className="nav-item active">
                        <BrowserRouter> <Link className="nav-link" to="/home_pgS">Logout</Link> </BrowserRouter>
                    </li>
                    <li className="nav-item">
                        <BrowserRouter> <Link className="nav-link" to="mailto:MissionCodeSupport@gmail.com">Support</Link> </BrowserRouter>
                    </li>
                </ul>
            </div>
        </nav>
    )
}
export default NavOther; 
