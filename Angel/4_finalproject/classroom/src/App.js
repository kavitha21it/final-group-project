import React from 'react';
import NavHome from './shared/navigation/nav_Home';
//import NavOther from './shared/navigation/nav_Other';
import HomePage from './pages/home/Home_pg'
import Athena from './dummyStuff/Athena';
import Footer from './shared/footer/footer';
import Joblist from './pages/Job_List/Job_List_pg';
import Input from './pages/Input/Input_pg'; 

import {
  BrowserRouter,
  Link,
  Route,
  Switch,
} from 'react-router-dom';

// 
// <NavOther/>
//<JobList/>
// <Route path="/Job_List_pg"> <JobList/> </Route>
// <Route path="/Input_pg"> <Input/> </Route>
// <Route path="/home_pg"> <HomePage /> </Route>
function App() {
  return (
    <BrowserRouter>
      
        <NavHome/>

        <Switch>

          <Route path="/Home_pg">
            <HomePage/>
          </Route>

          <Route path="/Job_List_pg">
            <Joblist/>
          </Route>

          <Route path="/Input_pg">
            <Input/>
            </Route>

          <Route path="/OMG">
          <Athena />
          </Route>
          
          <Route path="/">
          <HomePage/>
           </Route>
        </Switch>
        <Footer />
      
    </BrowserRouter>

  );
}

export default App;
