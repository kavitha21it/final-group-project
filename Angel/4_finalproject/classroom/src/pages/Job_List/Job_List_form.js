import React from 'react';


function JobListForm(props) {

    const [jobListFormVal, setJobListFormVal] = ['', '']; // React.useState();

    return (
        <div className="col-md-6">
            <div className="card border-secondary mb-3">
                <div className="card-header"> Quick Add - New Job Listing Form</div>
                <div className="card-body text-secondary">
                    <form onSubmit={handleFormSubmit}>
                        <div className="form-group row flex">
                            <label htmlFor="titleInput" className="col-sm-2 col-form-label">Title</label>
                            <input type="text" value={jobListFormVal.title} onChange={handleTitleChange} className="col-sm-10  form-control " id="titleInput" placeholder="Enter Job Title" />
                        </div>
                        <div className="form-group row">
                            <label htmlFor="titleDesc" className="col-sm-2 col-form-label" >Details</label>
                            <input type="text" value={jobListFormVal.desc} onChange={handleDescChange} className=" col-sm-10  form-control" id="titleDesc" placeholder="Enter Job Description Details" />
                        </div>
                        <div className="form-group row">
                            <label htmlFor="titleDesc" className="col-sm-2 col-form-label">Location</label>
                            <input type="text" value={jobListFormVal.location} onChange={handleLocationChange} className="col-sm-10  form-control" id="titleDesc" placeholder="Enter Job Location" />
                        </div>
                       <button type="submit" className="btn view border-info ml-auto text-white d-flex mt-auto">Quick Add</button>
                    </form>
                </div>
            </div>
        </div>

    );



    function handleFormSubmit($event) {
        $event.preventDefault();
        console.log("handle form submit", jobListFormVal);
        props.recordChange(jobListFormVal);
        setJobListFormVal(props.record);
    
    //const oldJobList = jobsList;
    //const newJobList = [jobListFormVal]
    //const newJobLists = oldJobList.concat(newJobList)
    //setJobList(newJobList);
    //setJobListFormVal(initialJobList);
    //}

    function handleTitleChange($event) {
        setJobListFormVal({
            title: $event.target.value,
            location: jobListFormVal.location,
            desc: jobListFormVal.desc,
        })
    }

    function handleDescChange($event) {
        setJobListFormVal({
            title: jobListFormVal.title,
            location: $event.target.value,
            desc: jobListFormVal.desc,
        })
    }

    function handleLocationChange($event) {
        setJobListFormVal({
            title: jobListFormVal.title,
            location: jobListFormVal.location,
            desc: $event.target.value,
        })
    }

};

export default JobListForm;