import React from 'react';
import '../home/mystyle.css';


function Joblist(props) {


    function ListJobs(jobs) {
        const jobsMapped = jobs.map(function (job) {


            return (
                <div className="col-sm-6 col-md-6 col-lg-6">
                    <div>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="card borderpg2 ml-1 mr-1 mt-3">
                                    <div className="card-body d-flex flex-column" style={{ "maxHeight": 214 }}>
                                        <a className="card-title h5 title" href="#" data-toggle="modal"
                                            data-target="#entry-level-software-application">{job.title}</a>
                                        <a className="card-sub title mb-2 h6 ml-1 row text-muted mt-2">{job.location}</a>
                                        <p className="card-text justify-content">{job.desc}</p>
                                        <div className="d-flex mt-auto mb-1">
                                            <a href="Input_pg.html" className="btn apply text-light ">Apply</a>
                                            <a href="#" className="btn view border-info ml-auto text-white " data-toggle="modal"
                                                data-target="#entry-level-software-application">View Details</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal fade" id="entry-level-software-application" tabindex="-1" role="dialog"
                        aria-labelledby="entry-level-software-application-title" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="entry-level-software-application-title">
                                        {job.title}</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body justify-content">
                                    <h1>{job.desc}</h1>
                                    <img src="#"
                                        style={{ "width": 250 }} />
                                </div>
                                <div className="modal-footer d-flex mt-auto">
                                    <a href="Input_pg.html" className="btn apply text-light ">Apply</a>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            )
        });
        return jobsMapped;
    }
    console.log('working?');

    const initialJobList = [
        {
            title: "Entry level Software Engineer",
            location: "Charlotte, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Junior Java developer",
            location: "Mathews, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Java Developer",
            location: "Mooresville, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "SAS Developer",
            location: "Cary, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Salesforce Developer",
            location: "Charlotte, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Junior Salesforce Developer",
            location: "Fayetteville, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Marketing Developer",
            location: "Durham, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Mobile Developer",
            location: "Charlotte, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },

        {
            title: "Middle-Tier Developer",
            location: "Greensboro, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Graphics Developer",
            location: "Ashcville, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Big Data Developer",
            location: "Matthews, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",


        }
    ];

    /*function jobLists( ) { */

    /*const [jobsList, setJobList] = React.useState(/*initialJobList*/ /*);

   const initialNewJobList = {
       title: "",
    location: " ",
        desc: " "

    };*/

    /*   React.useEffect(() => {
           const jobListRef = window.firebase.database().ref('/Job_List_pg')
       jobListRef.on('value', function (snapshot) {
           const jobListsFromDB = snapshot.val(); 
           setjobLists(jobListsFromDB); 
           console.log ('jobLists from db', jobListsFromDB); 
           
       }); 
   }, 
   []); */



   /*
function handlefirstnameChange($event) {
    const formValue = {
        firstname: $event.target.value,
        lastname: jobformvalue.lastname,
        email: jobformvalue.email,
        primaryphone: jobformvalue.primaryphone,
        secondaryphone: jobformvalue.secondaryphone,
        linkedinprofile: jobformvalue.linkedinprofile,
        resume: jobformvalue.resume
    };

    setFormVal(formValue);
}
function handlelastnameChange($event) {
    const formValue = {
        firstname: jobformvalue.firstname,
        lastname: $event.target.value,
        email: jobformvalue.email,
        primaryphone: jobformvalue.primaryphone,
        secondaryphone: jobformvalue.secondaryphone,
        linkedinprofile: jobformvalue.linkedinprofile,
        resume: jobformvalue.resume
    };

    setFormVal(formValue);
}
function handleemailChange($event) {
    const formValue = {
        firstname: jobformvalue.firstname,
        lastname: jobformvalue.lastname,
        email: $event.target.value,
        primaryphone: jobformvalue.primaryphone,
        secondaryphone: jobformvalue.secondaryphone,
        linkedinprofile: jobformvalue.linkedinprofile,
        resume: jobformvalue.resume
    };

    setFormVal(formValue);
}
function handleprimaryphoneChange($event) {
    const formValue = {
        firstname: jobformvalue.firstname,
        lastname: jobformvalue.lastname,
        email: jobformvalue.email,
        primaryphone: $event.target.value,
        secondaryphone: jobformvalue.secondaryphone,
        linkedinprofile: jobformvalue.linkedinprofile,
        resume: jobformvalue.resume
    };

    setFormVal(formValue);
}
function handlesecondaryphoneChange($event) {
    const formValue = {
        firstname: jobformvalue.firstname,
        lastname: jobformvalue.lastname,
        email: jobformvalue.email,
        primaryphone: jobformvalue.primaryphone,
        secondaryphone: $event.target.value,
        linkedinprofile: jobformvalue.linkedinprofile,
        resume: jobformvalue.resume
    };

    setFormVal(formValue);
}
function handlelinkedinprofileChange($event) {
    const formValue = {
        firstname: jobformvalue.firstname,
        lastname: jobformvalue.lastname,
        email: jobformvalue.email,
        primaryphone: jobformvalue.primaryphone,
        secondaryphone: jobformvalue.secondaryphone,
        linkedinprofile: $event.target.value,
        resume: jobformvalue.resume
    };

    setFormVal(formValue);
}
function handleresumeChange($event) {
    const formValue = {
        firstname: jobformvalue.firstname,
        lastname: jobformvalue.lastname,
        email: jobformvalue.email,
        primaryphone: jobformvalue.primaryphone,
        secondaryphone: jobformvalue.secondaryphone,
        linkedinprofile: jobformvalue.linkedinprofile,
        resume: $event.target.value
    };

    setFormVal(formValue);
}
function handleFormSubmit($event) {
    $event.preventDefault();
    console.log(jobformvalue);
    const oldformvalue = jobformvalue;
    const updatedRecord = {};
    updatedRecord['/applicant/' + oldformvalue.length] = jobformvalue;
    window.firebase.database().ref().update(updatedRecord);

}

*/

    /*const [jobListFormVal, setJobListFormVal] = React.useState(initialJobList);

   console.log(props);
    return (
  <div>
    <div className="container-fluid">
    <div className="row">
     <JobListForm record={initialNewJobList} recordChange={handleNewJobList} />
     {ListJobs(jobListFormVal)}
    </div>
        </div>
        </div>
    )*/

    /*function handleFormSubmit(jobListFormVal) {
       console.log('Handle new jobList', jobListFormVal);
      $event.preventDefault();
       const oldJobList = jobsList;
    const newJobList = [jobListFormVal]
    const newJobLists = oldJobList.concat(newJobList)
    setJobList(newJobList);
    setJobListFormVal(initialJobList);
    //}*/

   /* function handleNewJobList() {

    }
    function handleTitleChange($event) {
     setJobListFormVal({
     title: $event.target.value,
     location: jobListFormVal.location,
     desc: jobListFormVal.desc,
       })
    } */

    /* function handleTitleChange($event) {
        setJobListFormVal({
       title: jobListFormVal.title,
       location: $event.target.value,
       desc: jobListFormVal.desc,
       })
    } */

    /* function handleTitleChange($event) {
    setJobListFormVal({
    title: jobListFormVal.title,
       location: jobListFormVal.location,
      desc: $event.target.value, 
     })
     
    }*/
};


export default Joblist;

