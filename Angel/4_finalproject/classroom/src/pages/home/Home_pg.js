import React from 'react';
import { BrowserRouter } from "react-router-dom";
import {
    Link
} from 'react-router-dom';





function HomePage() {
    return (
        <main>
            <div className="d-flex flex-column align-items-center justify-content-center align-middle ">
                <div className="mt-5 pt-5">
                    <img src="http://www.missioncode.co/img/logo.png" alt="missioncode logo" width="400" height="150" />
                </div>
                <div>
                    <form className="form-inline my-2 my-lg-0">
                        <input className="form-control mr-sm-2 " type="search"
                            placeholder="Search for job postings,job title or company" aria-label="Search" size="50" />
                        <Link className="btn btn-outline-info my-2 my-sm-0" to="/Home_pg">Search <span className="sr-only">(current)</span></Link>
                    </form>
                </div>
            </div>
        </main>
    );
}

export default HomePage;
