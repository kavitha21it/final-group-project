import React, { Component } from 'react'
import userdetails from './userdetails'

export class Form extends Component {

    state = {

        step: 1,
        firstname: '',
        lastname: '',
        email: '',
        occupation: '',
        city: '',
        bio: '',
    }

    // Proceed to next step
    nextStep = ()=> {
        const { step } = this.state;
        this.setState ({
            step: step + 1
        });
    }   
     // Go back to prev step
     prevStep = ()=> {
        const { step } = this.state;
        this.setState ({
            step: step - 1
        });
    }   

    // Handle fields change
    handleChange = input => e => {
        this.setState({[input]: e.target.value})
    }
    
    render() {
        const { step } = this.state;
        const { firstname, lastname, email, occupation, city,bio} = this.state
        const values = {firstname, lastname, email, occupation, city,bio}
       
        switch(step) {
            case 1:
                return(
                    <userdetals 
                        nextStep={this.nextStep}
                        handleChange={this.handleChange}
                        values={values}
                    
                    />
                )
                case 2:
                    return <h1>formdetails</h1>
                case 3:
                    return <h1>confirm</h1>   
                case 4:
                    return <h1>success</h1>     
        }
    }
}

export default Form
