import React from 'react';

function Assignments() {
    const assignments = [
        {
            imageSrc: "https://placeimg.com/140/80/any",
            title: "Html Tags by category",
            desc: "Inline elements vs Block Elements",
            createdOn: "Sep 8th 2019"
        },
        {
            imageSrc: "https://placeimg.com/140/80/any",
            title: "Get Started",
            desc: "Button in html / css",
            createdOn: "Sep 10th 2019"
        },
        {
            imageSrc: "https://placeimg.com/140/80/any",
            title: "Html Inline elements",
            desc: "Html Inline elements",
            createdOn: "Sep 14th 2019"
        }
    ];

    return (
        <div>
            <h3>List of assignments</h3>
            <div className="container-fluid">
                <div className="row">
                <div className="col-3">
                    {

                        assignments.map((assignment, index) => {
                            return <div className="col-sm-12 col-md-6 col-lg-4" key={index}>
                                <div className="card">
                                    <img src={assignment.imageSrc} className="card-img-top" alt="" />
                                    <div className="card-body">
                                        <h5 className="card-title">{assignment.title}</h5>
                                        <p className="card-text">Created on : {assignment.createdOn}</p>
                                        <div className="d-flex">
                                            <a href="#" className="btn mr-auto btn-primary">UpVote</a>
                                            <a href="#" className="btn btn-info">View</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        })

                    }
                </div>
            </div>
        </div>
        </div>
    )
}

export default Assignments;