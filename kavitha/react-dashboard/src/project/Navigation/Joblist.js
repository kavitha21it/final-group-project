import React from 'react';
import Job_form from './Job_form';
import {
    Link, useHistory
} from 'react-router-dom';

function Joblistfn() {

    const history = useHistory();
    /*const InitialJobList = [
        {
            title: "Entry level Software Engineer",
            location: "Charlotte, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",
            salary: "$50000 -$60000",
            dateposted: "11/13/19",
            recruiter: "John",
            jobid:1

        },
        {
            title: "Junior Java developer",
            location: "Mathews, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",
            salary: "$50000 -$60000",
            dateposted: "11/13/19",
            recruiter: "John",
            jobid:2
        },
        {
            title: "Junior Salesforce Developer",
            location: "Fayetteville, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",
            salary: "$50000 -$60000",
            dateposted: "11/13/19",
            recruiter: "John",
            jobid:3
        },
        {
            title: "Full-Stack Developer",
            location: "Matthews, NC",
            desc: "Must be an expert at all things JavaScript, including frameworks and object oriented programming (OOP).",
            salary: "$50000 -$60000",
            dateposted: "11/14/19",
            recruiter: "Abraham",
            jobid:4
        },
        {
            title: "SAS Developer",
            location: "Cary, NC",
            desc: "Must be an expert at all things SAS, including frameworks and object oriented programming (OOP).",
            salary: "$60000 -$70000",
            dateposted: "11/13/19",
            recruiter: "Tracy",
            jobid:5
        },
        {
            title: "Salesforce Developer",
            location: "Charlotte, NC",
            desc: "Must be an expert at all things salesforce, including frameworks and object oriented programming (OOP).",
            salary: "$50000 -$60000",
            dateposted: "11/15/19",
            recruiter: "Ellen",
            jobid:6
        },
        {
            title: "Junior Salesforce Developer",
            location: "Fayetteville, NC",
            desc: "Must be an expert at all things JavaScript, including frameworks and object oriented programming (OOP).",
            salary: "$50000 -$55000",
            dateposted: "11/17/19",
            recruiter: "Tracy",
            jobid:7
        },
        {
            title: "Marketing Manager",
            location: "Durham, NC",
            desc: "Must be an expert at all things JavaScript, including frameworks and object oriented programming (OOP).",
            salary: "$50000 -$55000",
            dateposted: "11/15/19",
            recruiter: "Ellen",
            jobid:8
        }
    ];
    const [JobList, SetJobList] = React.useState(InitialJobList);*/

    const [JobList, SetJobList] = React.useState([]);

    React.useEffect(() => {

        // console.log(window.firebase.database());
        const jobRef = window.firebase.database().ref('/jobs');

        jobRef.on('value', function (snapshot) {
            const jobsFromDB = snapshot.val();
            SetJobList(jobsFromDB);
            //console.log('jobs from db', jobsFromDB);
        });

    }, []);



    return (

        <div className="row">
            {

                JobList.map((job, index) => {

                    return <React.Fragment key={index}>
                        <div className="col-sm-6 my-3" >
                            <div className="card shadow">
                                <div className="card-body">
                                    <h5 className="card-title">{job.title}</h5>
                                    <h6 className="card-subtitle mb-1 text-muted">{job.location}</h6>
                                    <p className="card-text">{job.description}</p>
                                    <button type="button" className="btn btn-outline-info" data-toggle="modal" data-target={'#example_' + index}>View Details</button>
                                    <Link className="btn ml-3 btn-info" to={{
                                        pathname: `/apply/${job.id}`,
                                        state: { jobid: job.id }
                                    }} role="button">Apply</Link>

                                </div>
                            </div>
                        </div>
                        <div className="modal fade" id={'example_' + index} role="dialog" aria-labelledby={'example_' + index} aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="exampleModalLabel">{job.title}</h5>
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div className="modal-body">
                                        <table className="table table-borderless">
                                            <tbody>
                                                <tr>
                                                    <th>Jobid:</th>
                                                    <td>{job.id}</td>
                                                </tr>
                                                <tr>
                                                    <th>Location:</th>
                                                    <td>{job.location}</td>
                                                </tr>
                                                <tr>
                                                    <th>Date Posted:</th>
                                                    <td>{job.date}</td>
                                                </tr>
                                                <tr>
                                                    <th>Recruiter:</th>
                                                    <td>{job.recruiter}</td>
                                                </tr>
                                                <tr>
                                                    <th>Salary:</th>
                                                    <td>${job.minSalary} - ${job.maxSalary}</td>
                                                </tr>
                                                <tr>
                                                    <th>Description:</th>
                                                    <td>{job.description}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="modal-footer">

                                        <button type="button" className="btn btn-outline-info" data-dismiss="modal">Close</button>
                                        <button className="btn btn-info" role="button" jobid={job.id} data-dismiss="modal" onClick={handleApply}>Apply</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                })
            }
        </div>

    );

    function handleApply($event) {
        const jobid = $event.target.attributes.jobid.value;
        // console.log(jobid);
        history.push(`/apply/${jobid}`);
    }
}

export default Joblistfn;