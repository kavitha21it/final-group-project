import React from 'react';
import {
    Link
} from 'react-router-dom';

function Input(props) {
    // console.log(props);
    React.useEffect(() => {
        const database = window.firebase.database();
        const jobRef = database.ref('/jobs');

        database.ref('/applicants').on('value', function (snapshot) {
            const applicantsFromDB = snapshot.val();
            setApplicants(applicantsFromDB);
        });

        jobRef.on('value', function (snapshot) {
            const jobsFromDB = snapshot.val();
            const jobId = getJobId();
            setActiveJob(jobsFromDB[jobId]);
            //console.log('jobs from db', jobsFromDB);
        });

    }, []);

    const defaultFormValue = {
        firstName: "",
        lastName: "",
        email: "",
        primaryPhone: "",
        secondaryPhone: "",
        linkedinProfile: "",
        resume: ""
    };
    /*const [jobformvalue, setFormVal] = React.useState(defaultFormValue);*/
    const [jobformvalue, setFormVal] = React.useState([]);
    const [applicants, setApplicants] = React.useState([]);
    const [activeJob, setActiveJob] = React.useState([]);

    // const [lastApplicantsKey, setLastApplicantKey] = React.useState([]);

    return (
        <div className="d-flex justify-content-center mt-5">
            <form className="w-75 shadow p-5" id="applyjobform" onSubmit={handleFormSubmit}>

                <h4 className="text-center border-bottom pb-4">
                    Apply to {activeJob.title}, {activeJob.location}
                </h4>


                <div className="form-group row my-4">
                    <label htmlFor="input-firstName"
                        className="col-sm-2 col-form-label text-right font-weight-bold">First Name<sup>*</sup></label>
                    <div className="col-sm-4">
                        <input type="text" required value={jobformvalue.firstName} onChange={handlefirstNameChange} className="form-control" id="input-firstName" placeholder="First Name" />
                    </div>
                    <label htmlFor="input-lastName"
                        className="col-sm-2 col-form-label text-right font-weight-bold">Last Name<sup>*</sup></label>
                    <div className="col-sm-4 m-0">
                        <input type="text" required value={jobformvalue.lastName} onChange={handlelastNameChange} className="form-control" id="input-lastName" placeholder="Last Name" />
                    </div>
                </div>

                <div className="form-group row my-4">
                    <label htmlFor="input-email"
                        className="col-sm-2 col-form-label text-right font-weight-bold">Email<sup>*</sup></label>
                    <div className="col-sm-10">
                        <input type="email" required value={jobformvalue.email} onChange={handleemailChange} className="form-control" id="input-email" placeholder="Email" />
                    </div>
                </div>

                <div className="form-group row my-4">
                    <label htmlFor="input-pri-phone"
                        className="col-sm-2 col-form-label text-right font-weight-bold">Primary
                    Phone </label>
                    <div className="col-sm-4">
                        <input type="tel" required value={jobformvalue.primaryPhone} onChange={handleprimaryPhoneChange} className="form-control" id="input-sec-phone" placeholder="(000) 000 0000" />
                    </div>
                    <label htmlFor="input-sec-phone"
                        className="col-sm-2 col-form-label text-right mr-auto font-weight-bold">Secondary
                    Phone</label>
                    <div className="col-sm-4">
                        <input type="tel" value={jobformvalue.secondaryPhone} onChange={handlesecondaryPhoneChange} className="form-control" id="input-sec-phone" placeholder="(000) 000 0000" />
                    </div>
                </div>

                <div className="form-group row my-4">
                    <label htmlFor="input-linkedin" className="col-sm-2 col-form-label text-right font-weight-bold">LinkedIn Profile</label>
                    <div className="col-sm-10">
                        <input type="text" required value={jobformvalue.linkedinProfile} onChange={handlelinkedinProfileChange} className="form-control" id="input-linkedin" placeholder="https://www.linkedin.com/in/profilename" />
                    </div>
                </div>

                <div className="form-group row my-4">
                    <label htmlFor="resume-upload"
                        className="col-sm-2 col-form-label text-right font-weight-bold">Resume<sup>*</sup></label>
                    <div className="custom-file file-field col-sm-8">
                        <input type="file" required value={jobformvalue.resume} onChange={handleresumeChange} className="" id="resume-upload" />
                    </div>
                </div>
                <div className="row border-top">

                    <div className="d-flex w-100 pt-4">

                        <Link className="btn btn-info my-2 my-sm-0 ml-3" to="/joblist"> Back to Jobs</Link>

                        <div className="ml-auto">

                            <a href="page1_vs2.html" className="btn btn-outline-info mr-3">Cancel</a>

                            <button className="btn btn-info my-2 my-sm-0" type="submit">Submit</button>

                        </div>

                    </div>

                </div>

                <div className="modal" role="dialog" id="submitModal">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" >Thank You for your submission</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <p>A Liason will be in touch with you about next steps.</p>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">ok</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    )
    function handlefirstNameChange($event) {
        const formValue = {
            firstName: $event.target.value,
            lastName: jobformvalue.lastName,
            email: jobformvalue.email,
            primaryPhone: jobformvalue.primaryPhone,
            secondaryPhone: jobformvalue.secondaryPhone,
            linkedinProfile: jobformvalue.linkedinProfile,
            resume: jobformvalue.resume
        };

        setFormVal(formValue);
    }
    function handlelastNameChange($event) {
        const formValue = {
            firstName: jobformvalue.firstName,
            lastName: $event.target.value,
            email: jobformvalue.email,
            primaryPhone: jobformvalue.primaryPhone,
            secondaryPhone: jobformvalue.secondaryPhone,
            linkedinProfile: jobformvalue.linkedinProfile,
            resume: jobformvalue.resume
        };

        setFormVal(formValue);
    }
    function handleemailChange($event) {
        const formValue = {
            firstName: jobformvalue.firstName,
            lastName: jobformvalue.lastName,
            email: $event.target.value,
            primaryPhone: jobformvalue.primaryPhone,
            secondaryPhone: jobformvalue.secondaryPhone,
            linkedinProfile: jobformvalue.linkedinProfile,
            resume: jobformvalue.resume
        };

        setFormVal(formValue);
    }
    function handleprimaryPhoneChange($event) {
        const formValue = {
            firstName: jobformvalue.firstName,
            lastName: jobformvalue.lastName,
            email: jobformvalue.email,
            primaryPhone: $event.target.value,
            secondaryPhone: jobformvalue.secondaryPhone,
            linkedinProfile: jobformvalue.linkedinProfile,
            resume: jobformvalue.resume
        };

        setFormVal(formValue);
    }
    function handlesecondaryPhoneChange($event) {
        const formValue = {
            firstName: jobformvalue.firstName,
            lastName: jobformvalue.lastName,
            email: jobformvalue.email,
            primaryPhone: jobformvalue.primaryPhone,
            secondaryPhone: $event.target.value,
            linkedinProfile: jobformvalue.linkedinProfile,
            resume: jobformvalue.resume
        };

        setFormVal(formValue);
    }
    function handlelinkedinProfileChange($event) {
        const formValue = {
            firstName: jobformvalue.firstName,
            lastName: jobformvalue.lastName,
            email: jobformvalue.email,
            primaryPhone: jobformvalue.primaryPhone,
            secondaryPhone: jobformvalue.secondaryPhone,
            linkedinProfile: $event.target.value,
            resume: jobformvalue.resume
        };

        setFormVal(formValue);
    }
    function handleresumeChange($event) {
        const formValue = {
            firstName: jobformvalue.firstName,
            lastName: jobformvalue.lastName,
            email: jobformvalue.email,
            primaryPhone: jobformvalue.primaryPhone,
            secondaryPhone: jobformvalue.secondaryPhone,
            linkedinProfile: jobformvalue.linkedinProfile,
            resume: $event.target.value
        };

        setFormVal(formValue);
    }

    function handleFormSubmit($event) {
        $event.preventDefault();
        console.log('Props:', props.location);

        const jobId = getJobId();
        const lastApplicantKey = getLastApplicantKey(applicants[jobId].applicants);
        const newApplicantKey = parseInt(lastApplicantKey) + 1;

        const updatedRecord = {};

        updatedRecord['/applicants/' + jobId + '/applicants/' + newApplicantKey] = jobformvalue;
        console.log(updatedRecord);
        // window.firebase.database().ref().update(updatedRecord);

        document.querySelector('#applyjobform').reset();

        window.jQuery('#submitModal').modal('show');

    }

    function getLastApplicantKey($applicants) {
        const applicantKeysArray = Object.keys($applicants);
        const lastApplicantKey = applicantKeysArray[applicantKeysArray.length - 1];
        console.log('lastApplicantKey', lastApplicantKey);
        return lastApplicantKey;
    }

    function getJobId() {
        const pathNames = props.location.pathname.split('/');
        const jobId = pathNames[pathNames.length - 1];
        return jobId;
    }

};

export default Input;
