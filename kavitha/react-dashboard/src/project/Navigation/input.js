import React from 'react';

function Input(props) {
    const [initialFormValue, setFormVal] = React.useState(props.record);
    return(
        <main>
            <div className="d-flex justify-content-center align-items-center w-auto min-vh-100 mt-lg-n4">
                <form className="w-75">
                    <div className="container-fluid flex-column text-center mt-1">
                        <p className="font-weight-bold lead">Entry level Software Engineer<br></br>
                            Charlotte, NC<br></br>
                            Description, etc
                        </p>
                    </div>
                    <div className="form-group row">
                        <label for="input-Firstname" className="col-sm-2 col-form-label text-right pt-3 pb-3 is-invalid is-valid font-weight-bold m-0">FirstName<sup>*</sup></label>
                        <div className="col-sm-4" m-0>
                        <input type="text" required value={initialFormValue.title} onChange={handlefirstnameChange}className="form-control" id="input-firstname" placeholder="First Name" />
                        </div>
                        </div>
                        </form>
                        </div>
                        </main>

    )};
    
    function handleFormSubmit($event){
        $event.preventDefault();
        console.log("handle form submit", assignmentFormVal);
        props.recordChange(initialFormValue);
        setFormVal(props.record);
    }
    function handlefirstnameChange($event) {
        setFormVal({
        firstname:$event.target.value,
        lastname: initialFormValue.lastname,
        email: initialFormValue.email,
        primaryphone: initialFormValue.primaryphone,
        secondaryphone: initialFormValue.secondaryphone,
        linkedinprofile:initialFormValue.linkedinprofile,
        resume:initialFormValue.resume
        })
    }

