const assignments = [
    {
        title: "Entry level Software Engineer",
        location: "Charlotte, NC",
        desc:"Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",
        salary: "$50000 -$60000",
        dateposted : "11/13/19",
        recruiter: "John"

    },
    {
        title: "Junior Java developer",
        location: "Mathews, NC",
        desc:"Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",
        salary: "$50000 -$60000",
        dateposted : "11/13/19",
        recruiter: "John"
    },
    {
        title: "Junior Salesforce Developer",
        location: "Fayetteville, NC",
        desc:"Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities." ,
        salary: "$50000 -$60000",
        dateposted : "11/13/19",
        recruiter: "John"
    },
    {
        title: "Full-Stack Developer",
        location: "Matthews, NC",
        desc:"Must be an expert at all things JavaScript, including frameworks and object oriented programming (OOP).", 
        salary: "$50000 -$60000",
        dateposted : "11/14/19",
        recruiter: "Abraham"
    },
    {
      title: "SAS Developer",
      location: "Cary, NC",
      desc:"Must be an expert at all things SAS, including frameworks and object oriented programming (OOP).", 
      salary: "$60000 -$70000",
      dateposted : "11/13/19",
      recruiter: "Tracy"
  },
  {
    title: "Salesforce Developer",
    location: "Charlotte, NC",
    desc:"Must be an expert at all things salesforce, including frameworks and object oriented programming (OOP).", 
    salary: "$50000 -$60000",
    dateposted : "11/15/19",
    recruiter: "Ellen"
},
{
  title: "Junior Salesforce Developer",
  location: "Fayetteville, NC",
  desc:"Must be an expert at all things JavaScript, including frameworks and object oriented programming (OOP).", 
  salary: "$50000 -$55000",
  dateposted : "11/17/19",
  recruiter: "Tracy"
},
{
title: "Marketing Manager",
location: "Durham, NC",
desc:"Must be an expert at all things JavaScript, including frameworks and object oriented programming (OOP).", 
salary: "$50000 -$55000",
dateposted : "11/15/19",
recruiter: "Ellen"
}
];
const cardsElement = document.getElementById("generatecards");
let cardContent = '';
assignments.forEach(element => {
    cardContent= cardContent + `<div class="col-sm-6">
    <div class="card mt-1 border-secondary">
        <div class="card-body">
            <h5 class="card-title">${element.title}</h5>
            <h6 class="card-subtitle mb-1 text-muted">${element.location}</h6>
            <p class="card-text">${element.desc}</p>
            <a  class= "btn btn-info" href="page3.html" role="button">Apply</a>            
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal">View Details</button>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">${element.title}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <table class="table table-borderless">
  <tr>
    <th>Location:</th>
    <td>${element.location}</td>
  </tr>
  <tr>
    <th>Date Posted:</th>
    <td>${element.dateposted}</td>
  </tr>
  <tr>
    <th>Recruiter:</th>
    <td>${element.recruiter}</td>
  </tr>
  <tr>
    <th>Salary:</th>
    <td>${element.salary}</td>
  </tr>
  <tr>
    <th>Description:</th>
    <td>${element.desc}</td>
  </tr>
</table>
        </div>
        <div class="modal-footer">
        <a  class= "button btn-success btn-sm" href="page3.html" role="button">Apply</a>
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>` 
});
cardsElement.innerHTML = cardContent;	



